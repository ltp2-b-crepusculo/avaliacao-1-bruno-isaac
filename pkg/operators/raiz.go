package operators

import (
	"errors"
	"math"
	"strconv"
	"strings"
)

type Raiz struct {
	expression string
	Resultado  float64
}

func (r Raiz) Calculate(expression string) (float64, error) {
	// Dividir a expressão nos operandos
	var operands []string
	switch {
	case strings.Contains(expression, "&"):
		operands = strings.Split(expression, "&")
	case strings.Contains(expression, "pow"):
		operands = strings.Split(expression, "pow")
	}

	// Converter os operandos para números
	operand1, err := strconv.ParseFloat(strings.TrimSpace(operands[0]), 64)
	if err != nil {
		return 0, errors.New("first operand is not a valid number")
	}

	operand2, err2 := strconv.ParseFloat(strings.TrimSpace(operands[1]), 64)
	if err2 != nil {
		return 0, errors.New("first operand is not a valid number")
	}

	//operand2, err := strconv.ParseFloat(operands[1], 64)
	//if err != nil {
	//	return 0, errors.New("second operand is not a valid number")
	//}

	// Realizar a subtração
	result := math.Pow(operand1, 1/operand2)

	return result, nil
}
