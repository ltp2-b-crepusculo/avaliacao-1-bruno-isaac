package operators

import (
	"errors"
	"strconv"
	"strings"
)

type Soma struct {
	expression string
	Resultado  float64
}

func (s Soma) Calculate(expression string) (float64, error) {
	// Dividir a expressão nos operandos

	var operands []string
	switch {
	case strings.Contains(expression, "+"):
		operands = strings.Split(expression, "+")
	case strings.Contains(expression, "sum"):
		operands = strings.Split(expression, "sum")
	}

	// Converter os operandos para números
	operand1, err := strconv.ParseFloat(strings.TrimSpace(operands[0]), 64)
	if err != nil {
		return 0, errors.New("first operand is not a valid number")
	}

	operand2, err := strconv.ParseFloat(strings.TrimSpace(operands[1]), 64)
	if err != nil {
		return 0, errors.New("second operand is not a valid number")
	}

	// Realizar a soma
	result := operand1 + operand2

	return result, nil
}
