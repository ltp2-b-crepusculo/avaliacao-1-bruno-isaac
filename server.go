package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strings"
	"time"
)

type JSONResponse struct {
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"result,omitempty"`
}

type OnionHandler struct{}

func (OnionHandler) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	// Verificar se o método da requisição é GET
	if req.Method != http.MethodGet {
		// Método não suportado, responder com código 405 (Method Not Allowed)
		respondJSON(res, structToJSONResponse(405, "Method Not Allowed", nil), http.StatusMethodNotAllowed)
		return
	}

	// Verificar o caminho da requisição
	if req.URL.Path != "/result" {
		// Caminho inválido, responder com código 404 (Not Found)
		respondJSON(res, structToJSONResponse(404, "Not Found", nil), http.StatusNotFound)
		return
	}

	// Verificar se o parâmetro "op" está presente na query da requisição
	op := req.URL.Query().Get("op")
	operator := strings.Fields(req.URL.Query().Get("op"))[1]
	if op == "" {
		// Parâmetro "op" não encontrado, responder com código 400 (Bad Request)
		respondJSON(res, structToJSONResponse(400, "Bad Request", nil), http.StatusBadRequest)
		return
	}

	// Verificar se a operação especificada está no mapa de operações
	operation, ok := operacoesMap[operator]
	if !ok {
		// Operação inválida, responder com código 400 (Bad Request)
		respondJSON(res, structToJSONResponse(400, "Invalid Operation", nil), http.StatusBadRequest)
		return
	}

	// Validar os valores da expressão
	err := validateValues(operator, op)
	if err != nil {
		// Valores inválidos, responder com código 400 (Bad Request)
		respondJSON(res, structToJSONResponse(400, err.Error(), nil), http.StatusBadRequest)
		return
	}

	// Executar a operação aritmética
	result, err := operation.Calculate(op)
	if err != nil {
		// Se ocorrer um erro ao calcular, responder com código 500 (Internal Server Error)
		respondJSON(res, structToJSONResponse(500, "Internal Server Error", nil), http.StatusInternalServerError)
		return
	}

	// Responder com o resultado da operação
	respondJSON(res, structToJSONResponse(200, "Success", result), http.StatusOK)
}

func respondJSON(res http.ResponseWriter, data interface{}, statusCode int) {
	res.Header().Set("Content-Type", "application/json")
	res.WriteHeader(statusCode)
	json.NewEncoder(res).Encode(data)
}
func structToJSONResponse(code int, message string, data interface{}) JSONResponse {
	return JSONResponse{
		Code:    code,
		Message: message,
		Data:    data,
	}
}

func StartServer() {

	s := &http.Server{
		Addr:         "127.0.0.1:8080",
		Handler:      OnionHandler{},
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	log.Fatal(s.ListenAndServe())
}
